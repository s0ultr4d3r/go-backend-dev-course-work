package main

import (
	"os"

	short "urlShortener/shortener"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func serve(a short.App) {
	a.Router = mux.NewRouter()
	a.Init()
	a.Run(":" + os.Getenv("EXPOSED_PORT"))
}

func main() {
	a := short.App{}
	a.DB = short.InitDb("postgres")
	serve(a)
	log.WithFields(log.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")
}
