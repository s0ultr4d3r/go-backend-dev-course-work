package shortener

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"strings"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

func InitDb(datasource string) *sql.DB {
	var connstring string = "user=postgres password=password dbname=postgres sslmode=disable"
	db, err := sql.Open("postgres", connstring)
	if err != nil {
		fmt.Printf("DB open: %v", err)
		log.Panicf("DB open: %v", err)
		panic(err)
	}
	defer db.Close()

	f, err := ioutil.ReadFile("postgres_init/create_table.sql")
	if err != nil {
		log.Fatalf("cannot read file: %v", err)
	}
	startRq := strings.Split(string(f), ";")
	for _, startRq := range startRq {
		res, err := db.Exec(startRq)
		if err != nil {
			log.Fatalf("Start req error: %v", err)
		}
		fmt.Printf("Started: \n \v", res)
	}
	return db
}
