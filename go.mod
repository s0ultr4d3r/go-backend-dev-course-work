module urlShortener

go 1.17

require github.com/sirupsen/logrus v1.8.1

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.3
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
